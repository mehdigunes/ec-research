import org.vu.contest.ContestSubmission;
import org.vu.contest.ContestEvaluation;

import java.util.Random;
import java.util.Properties;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.lang.Object;
import java.io.*;
import java.lang.Math;

import java.util.Comparator;

public class player11 implements ContestSubmission {
    Random rnd_;
    ContestEvaluation evaluation_;
    private int evaluations_limit_;
    int POPULATIONSIZE = 100;
    double sigma = 0.5;

    public player11() {
        rnd_ = new Random();
    }

    public void setSeed(long seed) {
        // Set seed of algortihms random process
        rnd_.setSeed(seed);
    }

    public void setEvaluation(ContestEvaluation evaluation) {
        // Set evaluation problem used in the run
        evaluation_ = evaluation;

        // Get evaluation properties
        Properties props = evaluation.getProperties();
        // Get evaluation limit
        evaluations_limit_ = Integer.parseInt(props.getProperty("Evaluations"));
        // Property keys depend on specific evaluation
        // E.g. double param = Double.parseDouble(props.getProperty("property_name"));
        boolean isMultimodal = Boolean.parseBoolean(props.getProperty("Multimodal"));
        boolean hasStructure = Boolean.parseBoolean(props.getProperty("Regular"));
        boolean isSeparable = Boolean.parseBoolean(props.getProperty("Separable"));

        // Do sth with property values, e.g. specify relevant settings of your algorithm
        if (isMultimodal) {
            // Do sth
        } else {
            // Do sth else
        }
    }

    public void run() {
        // Settings
        int parentCount = 5;
        int evals = 0;

        // Initialise population
        Individual[] population = sortPopulation(initialisePopulation(POPULATIONSIZE, 10));
        Individual[] parents = new Individual[parentCount];

        // double fitnessMem = population[0].fitness;
        System.out.println(parentCount);

        while (evals < evaluations_limit_) {
            printFitness(population); // FOR LOG

            // Parent selection
            Individual[] dummyPopulation = copyPopulation(population, 51);
            for (int i = 0; i < parentCount; i++) {
                int index = rouletteWheel(dummyPopulation);
                parents[i] = population[i];
                dummyPopulation[i].inputFitness(0);
            }

            // Recombination
            Individual[] children = mate(parents);
            Individual[] newPopulation = concatenatePopulation(population, children);

            // Survivor selection
            population = selectSurvivor(sortPopulation(newPopulation));

            Double fitness = population[0].fitness;
            evals++;
            // sigma = sigma * 0.99;
        }
    }

    public void printFitness(Individual[] pop) {
        for (int i = 0; i < pop.length; i++) {
            System.out.println(pop[i].fitness);
        }
    }

    // https://stackoverflow.com/questions/9961292/write-to-text-file-without-overwriting-in-java
    // public void writeFitnesses(double fitness) {
    // File log = new File("log.txt");
    // try {
    // FileWriter fileWriter = new FileWriter(log, true);

    // BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
    // bufferedWriter.write(Double.toString(fitness) + "\n");
    // bufferedWriter.close();

    // System.out.println("Done");
    // } catch (IOException e) {
    // System.out.println("COULD NOT LOG!!");
    // }
    // }

    public Individual[] copyPopulation(Individual[] population, int size) {
        Individual[] dummy = new Individual[size];
        for (int i = 0; i < size; i++) {
            dummy[i] = new Individual(population[i].genesSize);
            dummy[i].inputFitness(population[i].fitness);
            dummy[i].inputGenes(population[i].genes);
        }
        return dummy;
    }

    public Individual[] concatenatePopulation(Individual[] x, Individual[] y) {
        int length = x.length + y.length;
        Individual[] total = new Individual[length];
        for (int i = 0; i < x.length; i++) {
            total[i] = x[i];
        }
        for (int i = x.length; i < length; i++) {
            total[i] = y[i - x.length];
        }
        return total;
    }

    // Initialises population with random values
    public Individual[] initialisePopulation(int populationSize, int genesSize) {
        Individual[] population = new Individual[populationSize];
        for (int i = 0; i < populationSize; i++) {
            population[i] = new Individual(genesSize);
            population[i].inputFitness((double) evaluation_.evaluate(population[i].genes));
        }
        return population;
    }

    // Highest fitness is at the start of the array
    public Individual[] sortPopulation(Individual[] population) {
        Comparator<Individual> comparator = new FitnessComparator();
        Arrays.sort(population, comparator);
        return population;
    }

    public int rouletteWheel(Individual[] population) {
        Random rand = new Random();
        float select = rand.nextFloat();

        double totalFitness = getFitnessPopulation(population);
        float[] probabilities = new float[population.length];
        for (int i = 0; i < probabilities.length; i++) {
            probabilities[i] = (float) population[i].fitness / (float) totalFitness;
        }

        float beginBound = 0;
        for (int i = 0; i < probabilities.length; i++) {
            if (beginBound < select && select <= probabilities[i] + beginBound) {
                return i;
            }
            beginBound += probabilities[i];
        }
        System.out.println("ERROR");
        return 0;
    }

    // Get total fitness
    public double getFitnessPopulation(Individual[] population) {
        double totalFitness = 0;
        for (int i = 0; i < population.length; i++) {
            totalFitness += population[i].fitness;
        }
        return totalFitness;
    }

    // Randomly selects N individuals from the array for parent selection
    public Individual[] selectPotentialParents(Individual[] population, int N) {
        Individual[] potentialParents = new Individual[N];
        Random rand = new Random();
        for (int i = 0; i < N; i++) {
            int rand_gen = rand.nextInt(population.length - 1);
            potentialParents[i] = population[rand_gen];
        }
        return potentialParents;
    }

    // Creates new array of POPULATIONSIZE highest fitness individuals
    public Individual[] selectSurvivor(Individual[] sortedPop) {
        Individual[] newPop = new Individual[POPULATIONSIZE];
        for (int i = 0; i < POPULATIONSIZE; i++) {
            newPop[i] = sortedPop[i];
        }
        return newPop;
    }

    // Gives back total fitness of every individual in the population
    public double fitnessPopulation(Individual[] pop) {
        double totalFitness = 0;
        for (int i = 0; i < pop.length; i++) {
            totalFitness += (double) evaluation_.evaluate(pop[i].genes);
        }
        return totalFitness;
    }

    // Mating
    public Individual[] mate(Individual[] parents) {
        Individual[] children = new Individual[parents.length];
        double[] genes = new double[parents[0].genesSize];
        for (int i = 0; i < children.length; i++) {
            genes = discretecrossover(parents);
            // genes = arithematiccrossover(parents);
            children[i] = new Individual(parents[0].genesSize);
            // children[i].inputGenes(genes);
            // children[i].inputGenes(swapmutation(genes));
            // children[i].inputGenes(mutation(genes));
            children[i].inputGenes(gaussianmutation(genes));
            children[i].inputFitness((double) evaluation_.evaluate(children[i].genes));
            cycle(parents); // So as to create a new ordering of parents for the next child
        }

        return children;
    }

    // Cycles the ordering of the parents in the array
    private void cycle(Individual[] parents) {
        // Individual temp0 = new Individual(parents[0].genesSize);
        // temp0.inputGenes(parents[0].genes);
        // Individual temp1 = new Individual(parents[1].genesSize);
        // temp1.inputGenes(parents[1].genes);

        Individual temp0 = new Individual(parents[0]);
        Individual temp1 = new Individual(parents[1]);

        for (int i = 0; i < parents.length - 1; i++) {
            if (i > 0)
                temp1 = parents[i + 1];
            parents[i + 1] = temp0;
            temp0 = temp1;
        }
        parents[0] = new Individual(parents[parents.length - 1]);
    }

    // Diagonal multi parent crossover
    // private Individual[] diagonalCrossover(Individual[] parents) {
    // Random rand = new Random();
    // int[] breaks = new int[parents.length - 1];
    // for (int i = 0; i < breaks.length; i++) {

    // breaks[i] rand.nextInt(parents.length - 1);
    // }
    // }

    // Discrete crossover between N parents
    private double[] discretecrossover(Individual[] parents) {
        double[] genes = new double[parents[0].genes.length];
        Random rand = new Random();
        int choice;
        for (int i = 0; i < genes.length; i++) {
            choice = rand.nextInt(parents.length);
            genes[i] = parents[choice].genes[i];
        }

        return genes;
    }

    // Arithematic crossover between N parents
    private double[] arithematiccrossover(Individual[] parents) {
        double[] genes = new double[parents[0].genes.length];
        double weight = (double) 1.0 / parents.length; // we are taking uniform weight. SO each parent contributes
                                                       // uniformly
        Random rand = new Random();
        int choice = rand.nextInt(genes.length);
        for (int i = 0; i < genes.length; i++) {
            if (i < choice) {
                genes[i] = parents[0].genes[i];
            } else {
                int totalgenevalue = 0;
                for (int j = 0; j < parents.length; j++) {
                    totalgenevalue += parents[j].genes[i];
                }
                genes[i] = (float) totalgenevalue * weight;
            }
        }
        return genes;
    }

    // Assuming mutation does not happen while doing crossovers
    private double[] swapmutation(double[] child) {
        Random rand = new Random();
        float value = rand.nextFloat();
        if (value < 0.1) {
            double[] newChild = child.clone();
            int length = newChild.length;
            int r1 = rand.nextInt(length);
            int r2 = rand.nextInt(length);
            boolean checkValues = (r1 == r2);
            while (checkValues) {
                r1 = rand.nextInt(length);
                checkValues = (r1 == r2);
            }

            double r1Value = newChild[r1];
            newChild[r1] = newChild[r2];
            newChild[r2] = r1Value;

            return newChild;
        } else {
            return child;
        }
    }

    private double[] mutation(double[] child) {
        Random rand = new Random();
        float value = rand.nextFloat();

        if (value < 0.1) {
            int index = rand.nextInt(child.length);
            double mutation = ThreadLocalRandom.current().nextDouble(-sigma, sigma);
            child[index] = child[index] + mutation;
            if (child[index] > 5) {
                child[index] = 5;
            } else if (child[index] < -5) {
                child[index] = -5;
            }

        }

        return child;
    }

    private double[] gaussianmutation(double[] child) {
        Random rand = new Random();
        float value = rand.nextFloat();

        if (value < 0.3) {
            // http://www.nashcoding.com/2010/07/07/evolutionary-algorithms-the-little-things-youd-never-guess-part-1/
            int index = rand.nextInt(10);
            double mean = child[index];
            double x1 = rand.nextDouble();
            double x2 = rand.nextDouble();

            if (x1 == 0)
                x1 = 1;
            if (x2 == 0)
                x2 = 1;

            double y1 = Math.sqrt(-2.0 * Math.log(x1)) * Math.cos(2.0 * Math.PI * x2);
            double new_gene = y1 * sigma + mean;
            if (new_gene > 5) {
                new_gene = 5;
            } else if (new_gene < -5) {
                new_gene = -5;
            }
            child[index] = new_gene;
        }

        return child;
    }
}
