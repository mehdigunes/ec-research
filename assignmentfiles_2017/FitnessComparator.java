// https://stackoverflow.com/questions/683041/how-do-i-use-a-priorityqueue

import java.util.Comparator;

public class FitnessComparator implements Comparator<Individual>
{
    @Override
    public int compare(Individual x, Individual y)
    {
        if (x.fitness < y.fitness) {
            return 1;
        }
        if (x.fitness > y.fitness) {
            return -1;
        }
        return 0;
    }
}