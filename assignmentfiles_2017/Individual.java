import java.util.concurrent.ThreadLocalRandom;

public class Individual {
    int genesSize;
    double[] genes;
    double fitness;

    public Individual(int genesSize) {
        this.genesSize = genesSize;
        genes = new double[this.genesSize];
        for (int i = 0; i < this.genesSize; i++) {
            genes[i] = ThreadLocalRandom.current().nextDouble(-5, 5);
        }
    }

    public Individual(Individual copyIndividual) {
        this.genesSize = copyIndividual.genesSize;
        // for (int i = 0; i < this.genesSize; i++) {
        // this.genes[i] = copyIndividual.genes[i];
        // }
        this.genes = copyIndividual.genes;
        this.fitness = copyIndividual.fitness;
    }

    public void inputFitness(double fitness) {
        this.fitness = fitness;
    }

    public void inputGenes(double[] genes) {
        this.genes = genes;
    }
}