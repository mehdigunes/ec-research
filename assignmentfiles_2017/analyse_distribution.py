import numpy as np
import pickle
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import sys

parents = 0

def init():
    best = []
    avg = []
    with open("best.pkl", 'wb') as f:
        pickle.dump(best, f)
    with open("avg.pkl", 'wb') as f:
        pickle.dump(avg, f)

def main2():
    f = open("logall.txt", "r")
    lines = f.read().splitlines()
    lines = lines[:-4]
    lines = lines[1:]

    evaluations = [lines[x:x+100] for x in range(0, len(lines), 100)]
    best_fitnesses = []
    avg_fitnesses = []
    for evaluation in evaluations:
        best = sorted(map(float, evaluation))[-1]
        average = sum(map(float, evaluation)) / float(len(evaluation))
        best_fitnesses.append(best)
        avg_fitnesses.append(average)

    with open('best.pkl') as f:
        all_best = pickle.load(f);
    with open('avg.pkl') as g:
        all_avg = pickle.load(g);
    
    all_best.append(best_fitnesses);
    all_avg.append(avg_fitnesses);


    with open('best.pkl', 'w') as f:
        pickle.dump(all_best, f);
    with open('avg.pkl', 'w') as g:
        pickle.dump(all_avg, g);

def read():
    with open('best.pkl') as f:
        all_best = pickle.load(f);
    with open('avg.pkl') as g:
        all_avg = pickle.load(g);

    print(str(all_best))
    print(str(all_avg))
    print(len(all_best))
    print(len(all_avg))

def compute():
    with open('best.pkl') as f:
        runs_best = pickle.load(f);
    with open('avg.pkl') as g:
        runs_avg = pickle.load(g);

    evaluationCount = len(runs_best[0])

    avg_best_evaluations = []
    for x in range(0, evaluationCount):
        total_best = 0
        for run in runs_best:
            total_best += run[x]
        avg_best_evaluations.append(total_best / float(len(runs_best)))

    avg_avg_evaluations = []
    for x in range(0, evaluationCount):
        total_avg = 0
        for run in runs_avg:
            total_avg += run[x]
        avg_avg_evaluations.append(total_avg / float(len(runs_avg)))

    save(avg_best_evaluations, avg_avg_evaluations)

def save(best, avg):
    with open('best5p.pkl', 'w') as f:
        pickle.dump(best, f);
    with open('avg5p.pkl', 'w') as g:
        pickle.dump(avg, g);

def plotGraphs():
    with open('best2p.pkl') as f:
        best2 = pickle.load(f);
    with open('avg2p.pkl') as g:
        avg2 = pickle.load(g);
    with open('best3p.pkl') as f:
        best3 = pickle.load(f);
    with open('avg3p.pkl') as g:
        avg3 = pickle.load(g);
    with open('best4p.pkl') as f:
        best4 = pickle.load(f);
    with open('avg4p.pkl') as g:
        avg4 = pickle.load(g);
    with open('best5p.pkl') as f:
        best5 = pickle.load(f);
    with open('avg5p.pkl') as g:
        avg5 = pickle.load(g);

    plt.plot(range(0, len(best2)), best2, 'ro', markersize=2)
    plt.plot(range(0, len(best3)), best3, 'bo', markersize=2)
    plt.plot(range(0, len(best4)), best4, 'go', markersize=2)
    plt.plot(range(0, len(best5)), best5, 'yo', markersize=2)
    plt.ylabel('Best fitness')
    plt.xlabel('Evaluation')
    plt.title('BentCigarFunction')
    plt.ylim(0, 10) # Bentcigar
    plt.show()

    plt.plot(range(0, len(avg2)), avg2, 'ro', markersize=2)
    plt.plot(range(0, len(avg3)), avg3, 'bo', markersize=2)
    plt.plot(range(0, len(avg4)), avg4, 'go', markersize=2)
    plt.plot(range(0, len(avg5)), avg5, 'yo', markersize=2)
    plt.ylabel('Average fitness')
    plt.xlabel('Evaluation')
    plt.title('BentCigarFunction')
    plt.ylim(0, 10) # Bentcigar
    plt.show()

# def main():
#     global parents
#     # f_1 = open("log1.txt", "r")
#     # f_2 = open("log2.txt", "r")
#     # f_3 = open("log3.txt", "r")
#     # f_4 = open("log4.txt", "r")
#     f_1 = open("log1kat.txt", "r")
#     f_2 = open("log2kat.txt", "r")
#     f_3 = open("log3kat.txt", "r")
#     f_4 = open("log4kat.txt", "r")
#     # f_1 = open("log1sch.txt", "r")
#     # f_2 = open("log2sch.txt", "r")
#     # f_3 = open("log3sch.txt", "r")
#     # f_4 = open("log4sch.txt", "r")
#     f1_lines = f_1.read().splitlines()
#     f2_lines = f_2.read().splitlines()
#     f3_lines = f_3.read().splitlines()
#     f4_lines = f_4.read().splitlines()
#     lines1 = f1_lines[:-4]
#     lines2 = f2_lines[:-4]
#     lines3 = f3_lines[:-4]
#     lines4 = f4_lines[:-4]
#     lines1 = lines1[1:]
#     lines2 = lines2[1:]
#     lines3 = lines3[1:]
#     lines4 = lines4[1:]
#     parents = lines1[0]

#     evaluations1 = [lines1[x:x+100] for x in range(0, len(lines1), 100)]
#     evaluations2 = [lines2[x:x+100] for x in range(0, len(lines2), 100)]
#     evaluations3 = [lines3[x:x+100] for x in range(0, len(lines3), 100)]
#     evaluations4 = [lines4[x:x+100] for x in range(0, len(lines4), 100)]

#     evaluations = [evaluations1, evaluations2, evaluations3, evaluations4]

#     # graph_evals = map(int, sys.argv[1:])
#     param = sys.argv[1:]

#     if param[0] == 'best':
#         graphBestIndividual(evaluations)
#     elif param[0] == 'avg':
#        graphWeightedAverage(evaluations)
#     elif param[0] == 'dist':
#         graphEvaluation(evaluations, map(int, param[1:]))
#     elif param[0] == 'distGroup':     
#         groupedFitness(evaluations, map(int, param[1:]))

# # Best individual over evaluations
# def graphBestIndividual(evaluations):
#     types = ['ro', 'bo', 'go', 'yo']
#     red_patch = mpatches.Patch(color='red', label='2 parents')
#     blue_patch = mpatches.Patch(color='blue', label='3 parents')
#     green_patch = mpatches.Patch(color='green', label='4 parents')
#     yellow_patch = mpatches.Patch(color='yellow', label='5 parents')
#     plt.legend(handles=[red_patch, blue_patch, green_patch, yellow_patch])
#     for evaluations_p in evaluations:
#         fitnesses = []
#         for evaluation in evaluations_p:
#             fitnesses.append(sorted(map(float, evaluation))[0])
#         plt.plot(range(0, len(fitnesses)), fitnesses, types[evaluations.index(evaluations_p)], markersize=2)
#     plt.ylabel('Best fitness')
#     plt.xlabel('Evaluation')
#     # plt.title('BentCigarFunction')
#     # plt.title('SchaffersEvaluation')
#     plt.title('KatsuuraEvaluation')
#     # plt.ylim(0, 10) # Bentcigar
#     # plt.ylim(0, 0.13) # Schaffers
#     plt.ylim(0, 1.15) # Katsuura
#     plt.show()
    
# # Average fitness over evaluations
# def graphWeightedAverage(evaluations):
#     types = ['ro', 'bo', 'go', 'yo']
#     red_patch = mpatches.Patch(color='red', label='2 parents')
#     blue_patch = mpatches.Patch(color='blue', label='3 parents')
#     green_patch = mpatches.Patch(color='green', label='4 parents')
#     yellow_patch = mpatches.Patch(color='yellow', label='5 parents')
#     plt.legend(handles=[red_patch, blue_patch, green_patch, yellow_patch])

#     for evaluations_p in evaluations:
#         weightedFitnesses = []
#         for evaluation in evaluations_p:
#             weightedFitnesses.append(sum(map(float, evaluation)) / len(evaluation))
#         plt.plot(range(0, len(evaluations_p)), weightedFitnesses, types[evaluations.index(evaluations_p)], markersize=2)
#     plt.ylabel('Average fitness')
#     plt.xlabel('Evaluation')
#     # plt.title('BentCigarFunction')
#     # plt.title('SchaffersEvaluation')
#     plt.title('KatsuuraEvaluation')
#     # plt.ylim(0, 10) # Bentcigar
#     # plt.ylim(0, 0.13) # Schaffers
#     plt.ylim(0, 1.15) # Katsuura
#     plt.show()

# # Regular distribution
# def graphEvaluation(evaluations, evals):
#     for graph_eval in evals:
#         evaluation = map(float, evaluations[graph_eval])
#         # print(evaluation)
#         plt.plot(range(0, len(evaluation)), evaluation, 'ro', markersize=1)
#         plt.ylabel('Fitness')
#         plt.xlabel('Individual')
#         plt.title('Evaluation ' + str(graph_eval))
#         plt.ylim(0, 10) # Can be changed to suit lower values!
#         plt.show()

# # Grouped distribution
# def groupedFitness(evaluations, evals):
#     for graph_eval in evals:
#         groups = [0] * 10
#         evaluation = map(float, evaluations[graph_eval])
#         for individual in evaluation:
#             for x in range(0, 10):
#                 if (individual >= x and individual < x + 1):
#                     groups[x] = groups[x] + 1
#                     break
#             if (individual == 10):
#                 groups[9] += groups[9] + 1

#         xaxis = []
#         for x in range(0, 10):
#             xaxis.append(str(x)+'-'+str(x+1))

#         plt.plot(xaxis, groups, 'ro')
#         plt.ylabel('Amount of individuals')
#         plt.xlabel('Fitness groups')
#         plt.title('Fitness distribution evaluation ' + str(graph_eval))
#         plt.ylim(1, 100) # Can be changed to suit lower values!
#         plt.show()

# init()
# main2()
# read()

# compute()
plotGraphs()