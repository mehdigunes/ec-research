import java.util.concurrent.ThreadLocalRandom;
import java.util.Random;

public class Mate {
    // There are N children from N parents
    public Individual[] mate(Individual[] parents) {
        Individual[] children = new Individual[parents.length];
        double[] genes = new double[parents[0].genesSize];
        for (int i = 0; i < children.length; i++) {
            genes = discretecrossover(parents);
            genes = arithematiccrossover(parents);
            children[0] = new Individual(parents[0].genesSize);
            children[0].inputGenes(mutation(genes));
            cycle(parents); // So as to create a new ordering of parents for the next child
        }

        return children;
    }

    // Cycles the ordering of the parents in the array
    private void cycle(Individual[] parents) {
        Individual temp0 = new Individual(parents[0]);
        Individual temp1 = new Individual(parents[1]);
        for (int i = 0; i < parents.length - 1; i++) {
            if (i > 0)
                temp1 = parents[i + 1];
            parents[i + 1] = temp0;
            temp0 = temp1;
        }
        parents[0] = Individual(parents[parents.length - 1]);
    }

    // Discrete crossover between N parents
    private double[] discretecrossover(Individual[] parents) {
        double[] genes = parents[0].genesSize;
        Random rand = new Random();
        int choice;
        for (int i = 0; i < genes.length; i++) {
            choice = rand.nextInt(parents.length - 1);
            genes[i] = parents[choice].genes[i];
        }

        return genes;
    }

    // Arithematic crossover between N parents
    private double[] arithematiccrossover(Individual[] parents) {
        double[] genes = parents[0].genesSize;
        int weight = (float) 1 / parents.length; // we are taking uniform weight. SO each parent contributes uniformly
        Random rand = new Random();
        int choice = rand.nextInt(genes.length);
        for (int i = 0; i < genes.length; i++) {
            if (i < choice) {
                genes[i] = parents[0].genes[i];
            } else {
                int totalgenevalue = 0;
                for (int j = 0; j < parents.length; j++) {
                    totalgenevalue += parents[j].genes[i];
                }
                genes[i] = (float) totalgenevalue * weight;
            }
        }
        return genes;
    }

    // Assuming mutation does not happen while doing crossovers
    private double[] mutation(double[] child) {
        double[] newChild = child.clone();
        int length = newChild.length;
        Random rand = new Random();
        int r1 = rand.nextInt(length);
        int r2 = rand.nextInt(length);
        boolean checkValues = (r1 == r2);
        while (checkValues) {
            r1 = rand.nextInt(length);
            checkValues = (r1 == r2);
        }

        double r1Value = newChild[r1];
        newChild[r1] = newChild[r2];
        newChild[r2] = r1Value;

        return newChild;
    }
}
