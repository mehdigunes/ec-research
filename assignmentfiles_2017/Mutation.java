import java.util.Arrays;
import java.util.Random;
import java.util.Properties;

public class Mutation {

    public static double[] Mutation(double[] child)
    {
        double[] newChild = child.clone();
        int length = newChild.length;
        Random rand = new Random();
        int r1 = rand.nextInt(length);
        int r2 = rand.nextInt(length);
        boolean checkValues = (r1 == r2);
        while(checkValues)
        {
            r1 = rand.nextInt(length);
            checkValues = (r1 == r2);
        }

        double r1Value = newChild[r1];
        newChild[r1] = newChild[r2];
        newChild[r2] = r1Value;

        return newChild;
    }

    public static void main(String[] args)
    {
        double[] child = {1, 2, 3, 4, 5, 6};
        double[] newChild = Mutation(child);
        System.out.println(Arrays.toString(newChild));
    }
}
